//
//  Fields.swift
//  IBMLabHomework
//
//  Created by Levente Szilágyi on 2019. 09. 10..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class Fields: Codable {
    
    var lat: Double
    var lon: Double
    var name: String
    var distanceInMeters: Double?
}
