//
//  Rows.swift
//  IBMLabHomework
//
//  Created by Levente Szilágyi on 2019. 09. 10..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class Rows: Codable {
    
    var id: String
    var fields: Fields
}
