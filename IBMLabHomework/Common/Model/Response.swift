//
//  Response.swift
//  IBMLabHomework
//
//  Created by Levente Szilágyi on 2019. 09. 10..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class Response: Codable {
    
    var total_rows: Int
    var bookmark: String
    var rows: [Rows]
}
