//
//  SearchViewViewControllerDelegate.swift
//  IBMLabHomework
//
//  Created by Levente Szilágyi on 2019. 09. 10..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import CoreLocation

protocol SearchViewViewControllerDelegate: class {
    func searchViewDidSearchForAirports(atLocation: CLLocationCoordinate2D, withRadius: CLLocationDistance)
}
