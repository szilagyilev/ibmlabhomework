//
//  SearchViewCoordinator.swift
//  IBMLabHomework
//
//  Created by Levente Szilágyi on 2019. 09. 10..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import CoreLocation
import UIKit

class SearchViewCoordinator: Coordinator {
    
    // MARK: - Private Properties
    private let presenter: UINavigationController
    private var searchViewController: SearchViewController?
    private var resultsViewCoordinator: ResultsViewCoordinator?
    
    // MARK: - Initializers
    init(withPresenter newPresenter: UINavigationController) {
        self.presenter = newPresenter
    }
    
    // MARK: - Public Functions
    func start() {
        let searchViewController = SearchViewController()
        searchViewController.delegate = self
        
        searchViewController.title = "Search Airports"
        presenter.pushViewController(searchViewController, animated: true)
        
        self.searchViewController = searchViewController
    }
}

// MARK: - Extensions
extension SearchViewCoordinator: SearchViewViewControllerDelegate {
    func searchViewDidSearchForAirports(atLocation newLocation: CLLocationCoordinate2D, withRadius newRadius: CLLocationDistance) {
        
        let resultsViewCoordinator = ResultsViewCoordinator(withPresenter: presenter,
                                                            atLocation: newLocation,
                                                            withRadius: newRadius)
        
        resultsViewCoordinator.start()
        self.resultsViewCoordinator = resultsViewCoordinator
    }
}
