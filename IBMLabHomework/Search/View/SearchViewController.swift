//
//  ViewController.swift
//  IBMLabHomework
//
//  Created by Levente Szilágyi on 2019. 09. 10..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import CoreLocation
import UIKit

class SearchViewController: UIViewController {
    
    // MARK: - Public Properties
    weak var delegate: SearchViewViewControllerDelegate?
    
    // MARK: - Private Properties
    private var locationManager = CLLocationManager()
    private var currentLocation: CLLocation!
    
    // MARK: Layout Elements
    private lazy var customInputAccessoryView: UIView = {
        
        let customInputAccessoryView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 44))
        customInputAccessoryView.backgroundColor = .white
        
        return customInputAccessoryView
    }()
    
    private lazy var customInputAccessoryViewDoneButton: UIButton = {
        
        let customInputAccessoryViewDoneButton = UIButton()
        
        customInputAccessoryViewDoneButton.setTitleColor(UIColor.init(rgb: 0x3333FA), for: .normal)
        customInputAccessoryViewDoneButton.setTitle("Done", for: .normal)
        customInputAccessoryViewDoneButton.addTarget(self, action: #selector(doneButtonPressed), for: .touchUpInside)
        
        return customInputAccessoryViewDoneButton
    }()
    
    private lazy var longitudeInputField: UITextField = {
        
        let longitudeInputField = UITextField()
        
        longitudeInputField.placeholder = "Longitude"
        longitudeInputField.textAlignment = .center
        longitudeInputField.borderStyle = .roundedRect
        longitudeInputField.keyboardType = .decimalPad
        latitudeInputField.returnKeyType = .done
        
        return longitudeInputField
    }()
    
    private lazy var latitudeInputField: UITextField = {
        
        let latitudeInputField = UITextField()
        
        latitudeInputField.placeholder = "Latitude"
        latitudeInputField.textAlignment = .center
        latitudeInputField.borderStyle = .roundedRect
        latitudeInputField.keyboardType = .decimalPad
        latitudeInputField.returnKeyType = .done
        
        return latitudeInputField
    }()
    
    private lazy var radiusInputField: UITextField = {
        
        let radiusInputField = UITextField()
        
        radiusInputField.placeholder = "Radius (km)"
        radiusInputField.textAlignment = .center
        radiusInputField.borderStyle = .roundedRect
        radiusInputField.keyboardType = .decimalPad
        radiusInputField.returnKeyType = .done
        
        return radiusInputField
    }()
    
    private lazy var currentLocationButton: UIButton = {
        
        let currentLocationButton = UIButton()
        
        currentLocationButton.backgroundColor = UIColor.init(rgb: 0x3333FA).withAlphaComponent(0.75)
        currentLocationButton.setTitleColor(.white, for: .normal)
        currentLocationButton.setTitle("Use current location", for: .normal)
        currentLocationButton.addTarget(self, action: #selector(currentLocationButtonPressed), for: .touchUpInside)
        currentLocationButton.isEnabled = true
        
        return currentLocationButton
    }()
    
    private lazy var searchButton: UIButton = {
        
        let searchButton = UIButton()
        
        searchButton.backgroundColor = UIColor.init(rgb: 0x3333FA)
        searchButton.setTitleColor(.white, for: .normal)
        searchButton.setTitle("Search", for: .normal)
        searchButton.addTarget(self, action: #selector(searchButtonPressed), for: .touchUpInside)
        searchButton.isEnabled = true
        
        return searchButton
    }()
    
    // MARK: - Public Methods
    // MARK: Lifecycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupViewAppearance()
        
        layoutViewElements()
        
        setupViewElements()
    }
    
    // MARK: - Private Methods
    // MARK: Layouting Methods
    private func layoutViewElements() {
        
        view.addSubview(latitudeInputField)
        layoutLatitudeInput()
        
        view.addSubview(longitudeInputField)
        layoutLongitudeInput()
        
        view.addSubview(radiusInputField)
        layoutRadiusInput()
        
        view.addSubview(currentLocationButton)
        layoutCurrentLocationButton()
        
        view.addSubview(searchButton)
        layoutSearchButton()
        
        customInputAccessoryView.addSubview(customInputAccessoryViewDoneButton)
        layoutCustomInputAccessoryViewDoneButton()
    }
    
    private func layoutLatitudeInput() {
        
        latitudeInputField.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                                  trailing: view.safeAreaLayoutGuide.trailingAnchor,
                                  bottom: nil,
                                  leading: view.safeAreaLayoutGuide.leadingAnchor,
                                  padding: UIEdgeInsets(top: 64, left: 64, bottom: 0, right: 64))
    }
    
    private func layoutLongitudeInput() {
        
        longitudeInputField.anchor(top: latitudeInputField.bottomAnchor,
                                   trailing: latitudeInputField.trailingAnchor,
                                   bottom: nil,
                                   leading: latitudeInputField.leadingAnchor,
                                   padding: UIEdgeInsets(top: 12, left: 0, bottom: 0, right: 0))
    }
    
    private func layoutRadiusInput() {
        
        radiusInputField.anchor(top: longitudeInputField.bottomAnchor,
                                trailing: longitudeInputField.trailingAnchor,
                                bottom: nil,
                                leading: longitudeInputField.leadingAnchor,
                                padding: UIEdgeInsets(top: 12, left: 0, bottom: 0, right: 0))
    }
    
    private func layoutCurrentLocationButton() {
        currentLocationButton.anchor(top: radiusInputField.bottomAnchor,
                                     trailing: radiusInputField.trailingAnchor,
                                     bottom: nil,
                                     leading: radiusInputField.leadingAnchor,
                                     padding: UIEdgeInsets(top: 12, left: 0, bottom: 0, right: 0))
        
        currentLocationButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
    }
    
    private func layoutSearchButton() {
        
        searchButton.anchor(top: nil,
                            trailing: view.trailingAnchor,
                            bottom: view.safeAreaLayoutGuide.bottomAnchor,
                            leading: view.leadingAnchor)
        
        searchButton.heightAnchor.constraint(equalToConstant: 64).isActive = true
    }
    
    private func layoutCustomInputAccessoryViewDoneButton() {
        
        customInputAccessoryViewDoneButton.anchor(top: customInputAccessoryView.topAnchor,
                                                  trailing: customInputAccessoryView.trailingAnchor,
                                                  bottom: customInputAccessoryView.bottomAnchor,
                                                  leading: nil,
                                                  padding: UIEdgeInsets(top: 2, left: 0, bottom: 2, right: 16))
    }
    
    
    // MARK: Setup Methods
    
    func setupViewElements() {
        
        latitudeInputField.inputAccessoryView = customInputAccessoryView
        longitudeInputField.inputAccessoryView = customInputAccessoryView
        radiusInputField.inputAccessoryView = customInputAccessoryView
    }
    
    func setupViewAppearance() {
        
        view.backgroundColor = .white
        title = "Search Airports"
    }
    
    //MARK: - Event handlers
    
    @objc private func doneButtonPressed() {
        
        view.endEditing(true)
    }
    
    @objc private func currentLocationButtonPressed() {
        
        locationManager.requestWhenInUseAuthorization()
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            currentLocation = locationManager.location
            longitudeInputField.text = String(currentLocation.coordinate.longitude)
            latitudeInputField.text = String(currentLocation.coordinate.latitude)
            
        }
    }
    
    @objc private func searchButtonPressed() {
        
        let newLatitude = Double(latitudeInputField.text!) ?? 0
        let newLongitude = Double(longitudeInputField.text!) ?? 0
        let newRadius = Double(radiusInputField.text!) ?? 0
        delegate?.searchViewDidSearchForAirports(atLocation: CLLocationCoordinate2D.init(latitude: newLatitude, longitude: newLongitude), withRadius: newRadius)
    }
    
    // MARK: - Extensions
    
}



