//
//  ResultsViewController.swift
//  IBMLabHomework
//
//  Created by Levente Szilágyi on 2019. 09. 10..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import CoreLocation
import Foundation
import MapKit
import UIKit

class ResultsViewController: UIViewController {
    
    // MARK: - Public Properties
    var location: CLLocationCoordinate2D
    var radius: CLLocationDistance
    
    // MARK: - Private Properties
    private var model: ResultsModel
    private let remoteService: RemoteServiceImpl = RemoteServiceImpl()
    private let cellId = "resultCell"
    
    private var baseURLFormat: String = "https://mikerhodes.cloudant.com/airportdb/_design/view1/_search/geo?q=lon:[%@ TO %@] AND lat:[%@ TO %@]%@"
    // Example "https://mikerhodes.cloudant.com/airportdb/_design/view1/_search/geo?q=lon:[0%20TO%2030]%20AND%20lat:[0%20TO%205]"
    
    // MARK: Layout Elements
    private var resultTableView = UITableView()
    
    // MARK: - Initializers
    init(atLocation newLocation: CLLocationCoordinate2D, withRadius newRadius: CLLocationDistance) {
        
        location = newLocation
        radius = newRadius
        model = ResultsModel()
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public Methods
    // MARK: Lifecycle
    override func viewDidLoad() {
        
        retrieveNextItems()
        
        layoutComponents()
        setupComponents()
    }
    
    // MARK: - Private Methods
    
    private func retrieveNextItems() {
        
        var bookmarkUrl = ""
        
        if let lastBookmark = model.bookmarks.last {
            bookmarkUrl = "&bookmark=\(lastBookmark)"
        }
        
        let zone = MKCoordinateRegion(center: location, latitudinalMeters: radius*1000, longitudinalMeters: radius*1000)
        
        let minLatitude = location.latitude - zone.span.latitudeDelta/2
        let maxLatitude = location.latitude + zone.span.latitudeDelta/2
        let minLongitude = location.longitude - zone.span.longitudeDelta/2
        let maxLongitude = location.longitude + zone.span.longitudeDelta/2
        
        var newUrl = String(format: baseURLFormat, "\(minLatitude)", "\(maxLatitude)", "\(minLongitude)","\(maxLongitude)",bookmarkUrl)
        newUrl = newUrl.replacingOccurrences(of: " ", with: "%20")
        
        remoteService.fetchObjects(urlString: newUrl) {[unowned self] (object: Response) in
            self.model.rows.append(contentsOf: object.rows)
            self.model.bookmarks.append(object.bookmark)
            
            self.calculateDistances()
            self.sortLocations()
            
            DispatchQueue.main.async {[unowned self] in
                self.resultTableView.reloadData()
            }
        }
    }
    
    // MARK: Layouting Methods
    private func layoutComponents() {
        
        view.addSubview(resultTableView)
        layoutResultTableView()
    }
    
    private func layoutResultTableView() {
        
        resultTableView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                               trailing: view.safeAreaLayoutGuide.trailingAnchor,
                               bottom: view.safeAreaLayoutGuide.bottomAnchor,
                               leading: view.safeAreaLayoutGuide.leadingAnchor)
    }
    
    // MARK: Setup Methods
    private func setupComponents() {
        
        setupResultsTableView()
    }
    
    private func setupResultsTableView() {
        resultTableView.dataSource = self
        
        resultTableView.register(ResultTableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    
    // MARK: Helper Methods
    private func calculateDistances() {
        
        for newLocation in model.rows {
            
            let locationCoordinates = CLLocation(latitude: newLocation.fields.lat, longitude: newLocation.fields.lon)
            
            newLocation.fields.distanceInMeters = locationCoordinates.distance(from: CLLocation(latitude: location.latitude, longitude: location.longitude) )
        }
    }
    
    private func sortLocations() {
        model.rows.sort(by: {$0.fields.distanceInMeters! < $1.fields.distanceInMeters!})
    }
}

// MARK: - Extensions
extension ResultsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return model.rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // if reached last row, load next batch
        if indexPath.row == model.rows.count-1 {
            retrieveNextItems()
        }

        let cell = resultTableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        if (indexPath.row < model.rows.count-1) {
            cell.textLabel?.text = model.rows[indexPath.row].fields.name
            
            let distance = model.rows[indexPath.row].fields.distanceInMeters ?? 0
            cell.detailTextLabel?.text = "\(distance/1000) kms away"
        }
        
        return cell
    }
    
    
}
