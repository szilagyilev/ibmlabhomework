//
//  ResultsViewCoordinator.swift
//  IBMLabHomework
//
//  Created by Levente Szilágyi on 2019. 09. 10..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import CoreLocation
import Foundation
import UIKit

class ResultsViewCoordinator: Coordinator {
    
    // MARK: - Private Properties
    private let presenter: UINavigationController
    private var searchViewController: SearchViewController?
    private var resultsViewController: ResultsViewController?
    
    private var location: CLLocationCoordinate2D
    private var radius: CLLocationDistance

    
    // MARK: - Initializers
    init(withPresenter newPresenter: UINavigationController, atLocation newLocation: CLLocationCoordinate2D, withRadius newRadius: CLLocationDistance) {
        
        self.presenter = newPresenter
        self.location = newLocation
        self.radius = newRadius
    }
    
    // MARK: - Public Functions
    func start() {
        let resultsViewController = ResultsViewController.init(atLocation: location, withRadius: radius)

        resultsViewController.title = "Search Results"
        
        presenter.pushViewController(resultsViewController, animated: true)
        self.resultsViewController = resultsViewController
    }
}
